<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Publication;
use AppBundle\Model\PublicationQuery;
use AppBundle\Model\TransactionQuery;
use AppBundle\Model\PublicationPeer;
use AppBundle\Model\TransactionPeer;
use AppBundle\Model\CategoryPeer;
use AppBundle\Model\Authorhaspublication;
use AppBundle\Model\AuthorhaspublicationPeer;

class AppController extends Controller
{
	// Book sale weight index
	const BOOK_WEIGHT = 0.5;
	
	// Author sale weight index
	const AUTHOR_WEIGHT = 0.3;
	
	// Category sale weight index
	const CATEGORY_WEIGHT = 0.2;
	
	// top
	const TOP_RANKING = 10;
	
    /**
     * @Route(
     * 	"/{year}/{no_holidays}", 
     * 	name="woblink_rank", 
     * 	requirements={"id" = "\d+"}, 
     * 	defaults={"year" = 2015, "no_holidays" = false}
     * )
     */
    public function indexAction($year,$no_holidays)
    {
    	if(!preg_match('/^(20[0-9]{2})$/', $year))
    		$year = date('Y');
    	
    	$publications = false;
    	
    	// Get authors sale ranking in given period of time
    	$authors = TransactionQuery::create()
    		->select([TransactionPeer::ID])
    		->leftJoinPublication()
    		->leftJoin('Publication.Authorhaspublication')
    		->withColumn(AuthorhaspublicationPeer::AUTHOR_ID,'Author')
    		->withColumn('COUNT(*)','Sale')
    		->where('Transaction.status = ?', TransactionPeer::STATUS_ACCEPTED)
    		// holidays filter
    		->_if(!!$no_holidays)
    			->filterByDate(['min' => new \DateTime("{$year}-01-01"), 'max' => new \DateTime("{$year}-06-30")])
    			->_or()
    			->filterByDate(['min' => new \DateTime("{$year}-09-01"), 'max' => new \DateTime("{$year}-12-31")])
    		->_else()
    			->filterByDate(['min' => new \DateTime("{$year}-01-01"), 'max' => new \DateTime("{$year}-12-31")])
    		->_endif()
    		
    		->GroupBy('Author')
    		->find();

    	if($authors instanceof \PropelArrayCollection && !empty($authors))
    	{
    		// Prepare authors ranking
    		$authors_rank = [];
    		foreach($authors as $author)
    		{
    			$authors_rank[$author['Author']] = (int)$author['Sale'];
    		}
    	
	    	// Get all transactions in given period of time
	    	// No relations, just raw IDs
	    	$transactions = TransactionQuery::create()
	    		->select([TransactionPeer::ID])
	    		->leftJoinPublication()
	    		->leftJoin('Publication.Category')
	    		->leftJoin('Publication.Authorhaspublication')
	    		->withColumn(TransactionPeer::PUB_ID,'Publication')
	    		->withColumn('COUNT(DISTINCT '.TransactionPeer::ID.')','Sale')
	    		->withColumn(CategoryPeer::ID,'Category')	
	    		->withColumn('GROUP_CONCAT(DISTINCT '.AuthorhaspublicationPeer::AUTHOR_ID.' SEPARATOR \',\')','Authors')
	    		->where('Transaction.status = ?', TransactionPeer::STATUS_ACCEPTED)
	    		// holidays filter
	    		->_if(!!$no_holidays)
	    			->filterByDate(['min' => new \DateTime("{$year}-01-01"), 'max' => new \DateTime("{$year}-06-30")])
	    			->_or()
	    			->filterByDate(['min' => new \DateTime("{$year}-09-01"), 'max' => new \DateTime("{$year}-12-31")])
	    		->_else()
	    			->filterByDate(['min' => new \DateTime("{$year}-01-01"), 'max' => new \DateTime("{$year}-12-31")])
	    		->_endif()
	    		
	    		->groupBy(PublicationPeer::ID)
	    		->find();
	
	    	if($transactions instanceof \PropelArrayCollection && !empty($transactions))
	    	{
	    		// Anonymous function to get best author's index
	    		$getAuthorIndex = function($authors) use(&$authors_rank) {
	    			if(isset($authors_rank[$authors]))
	    				return (int)$authors_rank[$authors] * (float)$this->container->getParameter('weight_author',AppController::AUTHOR_WEIGHT);
	    			
	    			$arr = explode(',',$authors);
	    			
	    			$sale = 0;
	    			foreach($arr as $author)
	    			{
	    				if(isset($authors_rank[$author]) && (int)$authors_rank[$author] > $sale)
	    					$sale = (int)$authors_rank[$author];
	    			}
	    			
	    			if($sale > 0)
	    			{
	    				// cache result
	    				$authors_rank[$authors] = $sale;
	    				
	    				return $sale;
	    			}
	    			
	    			return 0;
	    		};
	    		
	    		// Preprare category ranking
	    		$category_rank = [];
	    		foreach($transactions as $transaction)
	    		{
	    			if(!isset($category_rank[$transaction['Category']]))
	    				$category_rank[$transaction['Category']] = 0;
	    		
	    			$category_rank[$transaction['Category']] += $transaction['Sale'];
	    		}
	    		
	    		// Preparing main list
	    		$list = [];
	    		foreach($transactions as $transaction)
	    		{
	    			// Book index
	    			$index = (int)$transaction['Sale'] * (float)$this->container->getParameter('weight_book',AppController::BOOK_WEIGHT);
	    		
	    			// Category index
	    			if(isset($category_rank[$transaction['Category']]))
	    				$index += (int)$category_rank[$transaction['Category']] * (float)$this->container->getParameter('weight_category',AppController::CATEGORY_WEIGHT);
	    		
	    			// Author index
	    			$index += (int)$getAuthorIndex($transaction['Authors']) * (float)$this->container->getParameter('weight_category',AppController::CATEGORY_WEIGHT);
	    		
	    			$list[$transaction['Publication']] = $index;
	    		}

	    		if(!empty($list))
	    		{
		    		// sort list by index
		    		asort($list,SORT_NUMERIC);
		    		
		    		// we need only top 10
		    		$list = array_slice($list,0,(int)$this->container->getParameter('top_ranking',AppController::TOP_RANKING),true);
	
		    		// the relevant query
		    		//$publications = PublicationQuery::create()->findPks(array_keys($list));
		    		
		    		$publications = PublicationQuery::create()
		    			->filterByPrimaryKeys(array_keys($list))
						->joinWith('Category')
		    			->find();
	    		}		    	
	    	}
    	}
 		
    	// render template
        return $this->render('default/index.html.twig',['publications' => $publications]);
    }
}

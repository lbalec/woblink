<?php

namespace AppBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'Publication' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.AppBundle.Model.map
 */
class PublicationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.AppBundle.Model.map.PublicationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Publication');
        $this->setPhpName('Publication');
        $this->setClassname('AppBundle\\Model\\Publication');
        $this->setPackage('src.AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 100, null);
        $this->addForeignKey('category_id', 'CategoryId', 'INTEGER', 'Category', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Category', 'AppBundle\\Model\\Category', RelationMap::MANY_TO_ONE, array('category_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Authorhaspublication', 'AppBundle\\Model\\Authorhaspublication', RelationMap::ONE_TO_MANY, array('id' => 'publication_id', ), 'CASCADE', 'CASCADE', 'Authorhaspublications');
        $this->addRelation('Transaction', 'AppBundle\\Model\\Transaction', RelationMap::ONE_TO_MANY, array('id' => 'pub_id', ), null, null, 'Transactions');
        $this->addRelation('Author', 'AppBundle\\Model\\Author', RelationMap::MANY_TO_MANY, array(), 'CASCADE', 'CASCADE', 'Authors');
    } // buildRelations()

} // PublicationTableMap
